#!/bin/bash
CODEDIR=""
for i in $(ls) 
do
    PYTHONPATH=$PYTHONPATH:/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/
    echo $PYTHONPATH
    if [ ${i: -3} == ".ui" ]
    then
    echo "compiling python UI code for $i"
    NAME=$(basename $i .ui)
    NAME="${NAME}UI.py"
    pyuic $i > $NAME
    mv $NAME ../$CODEDIR
    echo "done for $i"
    fi
done
