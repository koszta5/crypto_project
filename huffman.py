'''
Created on Oct 20, 2012

@author: kosta
'''
import operator
from PyQt4 import QtCore
import time
class HuffmanEncoder(QtCore.QThread):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        QtCore.QThread.__init__(self)
        self.___encode_list = []
        self.___char_codes = []
        self._show_progress = False
        
    def set_show_progress(self, value):
        self._show_progress = value
        
    def set_encode_list(self, list_of_freq):
        self.___char_codes = []
        self.___encode_list = list_of_freq
        self.___original_encode_list = list(list_of_freq)
        self.normalize_list()
        self.count_percent()
        
    def get_char_codes(self):
        return self.___char_codes
        
    def count_percent(self):
        total_nr = len(self.___encode_list)
        self.percent = int((2/float(total_nr))*50.0)

    def get_chars(self):
        items = []
        for item in self.___char_codes:
            items.append(item[0])
        return items
    
    def get_occurence(self, char):
        for item in self.___original_encode_list:
            if item[0] == char:
                return item[1]
    
    def get_code(self, char):
        for item in self.___char_codes:
            if item[0] == char:
                return item[1]
        
    def normalize_list(self):
        new_list = []
        for item in self.___encode_list:
            if item[1] != 0:
                new_list.append(item)
        self.___encode_list = new_list
        for item in self.___encode_list:
            self.___char_codes.append((item[0], ""))
    
    def run(self):
        self.encode()
        
    def get_item_by_letter_key(self, key):
        if len(list(key)) == 1:
            for item in self.___char_codes:
                if item[0] == key:
                    return item
        else:
            items = []
            search_for = list(key)
            for search in search_for:
                for item in self.___char_codes:
                    if item[0] == search:
                        items.append(item)
            return items
            
            
    
    def encode(self):
        #print "encode list "  + str(self.___encode_list)
        if self._show_progress:
            self.emit(QtCore.SIGNAL("stepEncoded()"))
            time.sleep(0.1)
        item_1 = self.get_item_by_letter_key(self.___encode_list[0][0])
        try:
            item_2 = self.get_item_by_letter_key(self.___encode_list[1][0])
        except IndexError,e:
            self.emit(QtCore.SIGNAL("encoderDone()"))
            print self.___char_codes
            return
        if isinstance(item_1, list):
            for single_item_1 in item_1:
                if single_item_1[1]:
                    self.___char_codes[self.___char_codes.index(single_item_1)] = (single_item_1[0], single_item_1[1] + "1")
                else:
                    self.___char_codes[self.___char_codes.index(single_item_1)] = (single_item_1[0], "1")
        else:
            if item_1[1]:
                self.___char_codes[self.___char_codes.index(item_1)] = (item_1[0], item_1[1] + "1")
            else:
                self.___char_codes[self.___char_codes.index(item_1)] = (item_1[0], "1")
        if isinstance(item_2, list):
            for single_item_2 in item_2:
                if single_item_2[1]:
                    self.___char_codes[self.___char_codes.index(single_item_2)]  = (single_item_2[0], single_item_2[1] + "0")
                else:    
                    self.___char_codes[self.___char_codes.index(single_item_2)]= (single_item_2[0], "0")
        else:
            if item_2[1]:
                self.___char_codes[self.___char_codes.index(item_2)]  = (item_2[0], item_2[1] + "0")
            else:    
                self.___char_codes[self.___char_codes.index(item_2)]= (item_2[0], "0")
        new_char = ""        
        if isinstance(item_1, list) or isinstance(item_2, list):
            if isinstance(item_1, list):
                for item in item_1:
                    new_char = new_char + item[0]
            if isinstance(item_2, list):
                for item in item_2:
                    new_char = new_char + item[0]
            try:
                new_char = new_char + item_1[0]
            except TypeError,e:
                pass
            try: 
                new_char = new_char + item_2[0]
            except TypeError,e:
                pass
        else:
            new_char = item_1[0] + item_2[0]
        new_val = self.___encode_list[0][1] + self.___encode_list[1][1]
        
        self.___encode_list[0] = (new_char, new_val)
        del(self.___encode_list[1])
        self.___encode_list = sorted(self.___encode_list, key=lambda tup: tup[1])
        #print "encode list after"  + str(self.___encode_list)
        #print "char list after"  + str(self.___char_codes)
        self.encode()
        self.emit(QtCore.SIGNAL("encoderStep()"))
        
