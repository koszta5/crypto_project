'''
Created on Aug 29, 2012

@author: Pavel Kostelnik
'''
from PyQt4 import QtCore, QtGui
from table_with_encodingUI import Ui_Results

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class TableDialogWindow(QtGui.QDialog):
    '''
    classdocs
    '''


    def __init__(self, window):
        '''
        Constructor
        '''
        QtGui.QDialog.__init__(self)
        self.window = window
        self._ui = Ui_Results()
        self._ui.setupUi(self)
        self.connect_signals()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.exec_()
        
        
    def connect_signals(self):
        self._ui.closeBtn.clicked.connect(self.close)
        self.setup_table()
        
    def prepare_samples(self):
        self.data = []
        for char in self.window.encoder.get_chars():
            row = []
            letter = QtGui.QLabel()
            letter.setText(_fromUtf8(char))
            row.append(letter)
            
            code = QtGui.QLabel()
            code.setText(_fromUtf8(self.window.encoder.get_code(char)))
            row.append(code)
            
            occurence = QtGui.QLabel()
            occurence.setText(_fromUtf8(str(self.window.encoder.get_occurence(char))))
            row.append(occurence)
            
            self.data.append(row)
    
    
    def setup_table(self):
        self.prepare_samples()
        i = 0 
        for row in reversed(self.data):
            self._ui.tableWidget.insertRow(i)
            self._ui.tableWidget.setCellWidget(i,0, row[0])
            self._ui.tableWidget.setCellWidget(i,1, row[1])
            self._ui.tableWidget.setCellWidget(i,2, row[2])
            i = i + 1
        self._ui.tableWidget.resizeColumnsToContents()
        self._ui.tableWidget.resizeRowsToContents()
        