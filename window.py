'''
Created on Aug 28, 2012

@author: Pavel Kostelnik
'''
from PyQt4 import QtGui, QtCore
from main_windowUI import Ui_HuffmanEncoderWindow
import sys, os
from analyzer import Analyzer
from huffman import HuffmanEncoder
from table_dialog_window import TableDialogWindow
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class Window(QtGui.QMainWindow):
    '''
    classdocs
    '''


    def __init__(self, main):
        '''
        Constructor
        '''
        self.main = main
        self.analyzer = Analyzer()
        self.encoder = HuffmanEncoder()
        self.createApp()
        QtGui.QMainWindow.__init__(self)
        self._ui = Ui_HuffmanEncoderWindow()
        self._ui.setupUi(self)
        self.set_default_text()
        self.connect_signals()
        self.connect_analyzer_signals()
        self.connect_encoder_signals()
        self.output_codes = ["self.show_huffman_code", "self.show_huffman_translation"]
        self.len_field = {"2": 1, "3": 3, "4":3, "5":4, "6":5, "7": 6, "8": 7, "9": 10, "10": 11, "11":12, "12": 13}
        self.show()
        self.raise_()
        self.run()
        
    def run(self):
        """
        Make app exitable
        """
        sys.exit(self._QtApp.exec_())
        
    def createApp(self):
        """
        Creates a Qtapp that is necessary for the program to run
        """
        self._QtApp = QtGui.QApplication(sys.argv)
        
    def connect_signals(self):
        self._ui.encodeBtn.clicked.connect(self.encode_to_huffman)
        self._ui.showTableBtn.clicked.connect(self.show_encode_table)
        self._ui.showProgressChck.clicked.connect(self.change_show_progress)
        self._ui.fileChooseBtn.clicked.connect(self.choose_file)
        self._ui.outputSelector.currentIndexChanged.connect(self.encoder_done)
        
    def show_encode_table(self):
        self.results = TableDialogWindow(self)
        
    def change_show_progress(self):
        self.encoder.set_show_progress(self._ui.showProgressChck.isChecked())
    
        
    def encode_to_huffman(self):
        self._ui.progressBar.setValue(0)
        self._ui.encodedTextEdit.setPlainText("")
        self.analyzer.text_to_analyze = str(self._ui.plainTextEdit.toPlainText().toUtf8())
        self.analyzer.start()
        
    def connect_encoder_signals(self):
        self.connect(self.encoder, QtCore.SIGNAL("encoderDone()"), self.encoder_done)
        self.connect(self.encoder, QtCore.SIGNAL("encoderStep()"), self.encoder_step)
        self.connect(self.encoder, QtCore.SIGNAL("stepEncoded()"), self.show_huffman_step)
        
    def encoder_done(self):
        self._ui.showTableBtn.setEnabled(True)
        eval(self.output_codes[self._ui.outputSelector.currentIndex()] + "()")
        if self._ui.ouputFileCheckbox.isChecked():
            self.create_output_file()
            
    def create_output_file(self):
        list_from_gui = list(self._ui.plainTextEdit.toPlainText().toUtf8())
        text = ""
        for char in list_from_gui:
            text += self.encoder.get_code(char)
        out_file = open(os.path.join(os.path.expanduser("~"), "Desktop", "huffman_encoder_output.txt"), "w+")
        out_file.write(text)
        out_file.close()
        
    def show_huffman_code(self):
        text = ""
        list_from_gui = list(self._ui.plainTextEdit.toPlainText().toUtf8())
        for char in list_from_gui:
            text += ";" + self.encoder.get_code(char)
        self._ui.encodedTextEdit.setPlainText(text)
        self._ui.progressBar.setValue(100)
        
    def show_huffman_translation(self):
        text = ""
        list_from_gui = list(self._ui.plainTextEdit.toPlainText().toUtf8())
        letters_line = ""
        for i, char in enumerate(list_from_gui):
            if i % 6 == 0 and i != 0:
                text += "\n"
                text += letters_line
                letters_line = ""
                text += "\n"
            try:
                spaces = self.len_field[(str(len(self.encoder.get_code(char))))]
            except KeyError,e:
                spaces = len(self.encoder.get_code(char)) + 1
            letters_line += " " * int(spaces)
            if char == " ":
                letters_line += "_"
            else:
                letters_line += char
            letters_line += " " * (spaces)
            letters_line += "       "
            text += self.encoder.get_code(char) + "       "
        text += "\n"+letters_line
        self._ui.encodedTextEdit.setPlainText(text)
        self._ui.progressBar.setValue(100)
        
    def show_huffman_step(self):
        if self._ui.showProgressChck.isChecked():
            mystring = ""
            for mytup in self.encoder.get_char_codes():
                mystring += str(mytup)+"\n"
            print mystring
            self._ui.encodedTextEdit.setPlainText(mystring)
        
    def encoder_step(self):
        self._ui.progressBar.setValue(self._ui.progressBar.value() + self.encoder.percent)
     
    def connect_analyzer_signals(self):
        self.connect(self.analyzer, QtCore.SIGNAL("raiseProgressForAnalyze()"), self.raise_progress_by_one)
        self.connect(self.analyzer, QtCore.SIGNAL("analyzerDone()"), self.start_encoding)
        
    def raise_progress_by_one(self):
        self._ui.progressBar.setValue(self._ui.progressBar.value()+1)
        
    def start_encoding(self):
        self.encoder.set_encode_list(self.analyzer.sorted_dict)
        self.encoder.start()
        
    def set_default_text(self):
        text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. \n It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).  ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. he standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
        self._ui.plainTextEdit.setPlainText(text)    
        
    def choose_file(self):
        self.source_file = QtGui.QFileDialog.getOpenFileName(self, "Choose source file", os.path.expanduser("~"), "Various text files (*.txt *.xml *.html)")
        src_file = open(self.source_file)
        lines = src_file.readlines()
        text = "\n".join(lines)
        self._ui.plainTextEdit.setPlainText(text)
        src_file.close()
        
    def change_view(self):
        pass
    
    
        
        
            
        