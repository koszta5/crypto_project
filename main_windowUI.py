# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_window.ui'
#
# Created: Fri Jan 11 14:57:12 2013
#      by: PyQt4 UI code generator snapshot-4.9.5-3ad1e0f2a0f2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_HuffmanEncoderWindow(object):
    def setupUi(self, HuffmanEncoderWindow):
        HuffmanEncoderWindow.setObjectName(_fromUtf8("HuffmanEncoderWindow"))
        HuffmanEncoderWindow.resize(800, 600)
        self.centralwidget = QtGui.QWidget(HuffmanEncoderWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.plainTextEdit = QtGui.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.gridLayout.addWidget(self.plainTextEdit, 4, 0, 1, 2)
        self.label_2 = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 10, 0, 1, 1)
        self.encodedTextEdit = QtGui.QPlainTextEdit(self.centralwidget)
        self.encodedTextEdit.setObjectName(_fromUtf8("encodedTextEdit"))
        self.gridLayout.addWidget(self.encodedTextEdit, 13, 0, 1, 2)
        self.progressBar = QtGui.QProgressBar(self.centralwidget)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.gridLayout.addWidget(self.progressBar, 14, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.encodeBtn = QtGui.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.encodeBtn.setFont(font)
        self.encodeBtn.setObjectName(_fromUtf8("encodeBtn"))
        self.horizontalLayout.addWidget(self.encodeBtn)
        self.showProgressChck = QtGui.QCheckBox(self.centralwidget)
        self.showProgressChck.setObjectName(_fromUtf8("showProgressChck"))
        self.horizontalLayout.addWidget(self.showProgressChck)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.showTableBtn = QtGui.QPushButton(self.centralwidget)
        self.showTableBtn.setEnabled(False)
        self.showTableBtn.setObjectName(_fromUtf8("showTableBtn"))
        self.horizontalLayout.addWidget(self.showTableBtn)
        self.gridLayout.addLayout(self.horizontalLayout, 6, 0, 2, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.outputSelector = QtGui.QComboBox(self.centralwidget)
        self.outputSelector.setObjectName(_fromUtf8("outputSelector"))
        self.outputSelector.addItem(_fromUtf8(""))
        self.outputSelector.addItem(_fromUtf8(""))
        self.horizontalLayout_2.addWidget(self.outputSelector)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.gridLayout.addLayout(self.horizontalLayout_2, 11, 0, 2, 1)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.fileChooseBtn = QtGui.QPushButton(self.centralwidget)
        self.fileChooseBtn.setObjectName(_fromUtf8("fileChooseBtn"))
        self.horizontalLayout_3.addWidget(self.fileChooseBtn)
        self.ouputFileCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.ouputFileCheckbox.setObjectName(_fromUtf8("ouputFileCheckbox"))
        self.horizontalLayout_3.addWidget(self.ouputFileCheckbox)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.gridLayout.addLayout(self.horizontalLayout_3, 2, 0, 2, 1)
        HuffmanEncoderWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(HuffmanEncoderWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        HuffmanEncoderWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(HuffmanEncoderWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        HuffmanEncoderWindow.setStatusBar(self.statusbar)

        self.retranslateUi(HuffmanEncoderWindow)
        QtCore.QMetaObject.connectSlotsByName(HuffmanEncoderWindow)

    def retranslateUi(self, HuffmanEncoderWindow):
        HuffmanEncoderWindow.setWindowTitle(QtGui.QApplication.translate("HuffmanEncoderWindow", "Huffman encoder", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "Plain text", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "Huffman encoded", None, QtGui.QApplication.UnicodeUTF8))
        self.encodeBtn.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "Encode", None, QtGui.QApplication.UnicodeUTF8))
        self.showProgressChck.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "Show progress", None, QtGui.QApplication.UnicodeUTF8))
        self.showTableBtn.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "Show encoding table", None, QtGui.QApplication.UnicodeUTF8))
        self.outputSelector.setItemText(0, QtGui.QApplication.translate("HuffmanEncoderWindow", "Plain Huffman", None, QtGui.QApplication.UnicodeUTF8))
        self.outputSelector.setItemText(1, QtGui.QApplication.translate("HuffmanEncoderWindow", "Translation", None, QtGui.QApplication.UnicodeUTF8))
        self.fileChooseBtn.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "From file", None, QtGui.QApplication.UnicodeUTF8))
        self.ouputFileCheckbox.setText(QtGui.QApplication.translate("HuffmanEncoderWindow", "Create ouput file", None, QtGui.QApplication.UnicodeUTF8))

