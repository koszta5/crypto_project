'''
Created on Oct 20, 2012

@author: kosta
'''
import string
import operator
from PyQt4 import QtCore
class Analyzer(QtCore.QThread):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        QtCore.QThread.__init__(self)
        self.___char_counts = {}
        
        
    def run(self):
        self.analyze(self.text_to_analyze)
        
    def analyze(self, mystring):
        chars = list(mystring)
        no_of_chars = len(string.printable)
        analyze_percet = 25
        raise_at = int(no_of_chars/analyze_percet)
        for i,char in enumerate(string.printable):
            self.___char_counts[char] = chars.count(char)
            if i % raise_at == 0:
                self.emit(QtCore.SIGNAL("raiseProgressForAnalyze()"))
        sorted_dict = sorted(self.___char_counts.iteritems(), key=operator.itemgetter(1))
        self.sorted_dict = sorted_dict
        self.emit(QtCore.SIGNAL("analyzerDone()"))
        
        